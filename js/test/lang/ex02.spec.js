function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  test('Name too long', () => {
    const longName = () => {
      createUser(
        'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        'fake@email.com.br',
        '123456',
        '123456'
      );
    };
    expect(longName).toThrowError('validation error: invalid name size');
  });

  test('Invalid email', () => {
    const longName = () => {
      createUser('arnaldo', 'fake@email.cm.br', '123456', '123456');
    };
    expect(longName).toThrowError('validation error: invalid email format');
  });

  test('Invalid password', () => {
    const longName = () => {
      createUser('arnaldo', 'fake@email.com.br', '@%$!@#@#', '@%$!@#@#');
    };
    expect(longName).toThrowError('validation error: invalid password format');
  });

  test('Passwords not match', () => {
    const longName = () => {
      createUser('arnaldo', 'fake@email.com.br', '1234564', '1234565');
    };
    expect(longName).toThrowError(
      'validation error: confirmation does not match'
    );
  });
});
