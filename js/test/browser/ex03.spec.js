const test = require('ava');

function scaffoldStructure(document, data) {
  document.documentElement.innerHTML = '';

  const ul = document.createElement('ul');
  data.forEach(obj => {
    const li = document.createElement('li');
    const name = document.createElement('span');
    name.innerHTML = name.innerHTML + obj.name;
    name.className = 'name';
    name.style = 'font-weight: bold';
    li.append(name);

    const rest = document.createElement('span');
    rest.innerHTML = rest.innerHTML + ' - ' + obj.email;
    li.append(rest);

    ul.append(li);
  });

  document.body.appendChild(ul);
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  // Testando se possui uma lista nao ordenada
  const unionLists = document.getElementsByTagName('ul');
  t.is(unionLists.length > 0, true);

  // Testando se criou a pagina e se possui 3 classes CSS .name
  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, 3);
  t.is(nameNodes.length, data.length);

  // Testando se possui texto ordenado em ordem alfabetica
  var names = [];
  nameNodes.forEach(node => names.push(node.innerHTML));
  const sortedNames = names.sort();
  t.is(names, sortedNames);

  // Testando se possui o estilo correto
  const lists = document.querySelectorAll('li');
  lists.forEach(list => {
    const name = list.querySelector('.name');
    t.is(name.style['font-weight'] === 'bold', true);
  });
});
